#include <SPI.h>
#include <Wire.h>
#include "OLED_Driver.h"
#include "OLED_GUI.h"
#include "DEV_Config.h"
#include "Show_Lib.h"
#include "Debug.h"
#include <Wire.h>
#include <Adafruit_ADS1015.h>


// ==================== Calibration Parameters ====================================
// NOTE: All values should be in mV!
float VOLTAGE_ZERO = 0;                     // Update this value with the one when the length is 0.
float VOLTAGE_ONE_INCH = 100;               // Update this value with the one when the length is 1 inch.
// ================================================================================


#define BTN_PIN           6                            // Use D6 for the push button
#define READ_INTERVAL     20                           // Read data in 50Hz


int buttonState = HIGH;                                // the current reading from the input pin
int lastButtonState = HIGH;                            // the previous reading from the input pin
unsigned long lastDebounceTime = 0;                    // the last time the output pin was toggled
unsigned long debounceDelay = 200;                     // the debounce time; increase if the output flickers
unsigned long readTime = 0;
unsigned long lastPrintTime = 0;
unsigned long lastDisplayTime = 0;

float acc_val = 0;
float max_val = 0;
float min_val = 100000;
float cur_val = 0;
unsigned long cnt = 0;

Adafruit_ADS1015 ads;
uint16_t adc_val;


uint16_t readADC_MultipleValues(int ch);


void setup() {
  Serial.begin(9600);
  Serial.println(F("===== Starting IndividualSensorDisplay ====="));

  System_Init();
  OLED_SCAN_DIR OLED_ScanDir = SCAN_DIR_DFT;
  OLED_Init(OLED_ScanDir);
  OLED_ClearBuf();
  OLED_ClearScreen(OLED_BACKGROUND);

  pinMode(BTN_PIN, INPUT);

  ads.setGain(GAIN_TWOTHIRDS);                    // 2/3x gain +/- 6.144V  1 bit = 0.1875mV (default)
  ads.begin();

}

void loop() {

  readTime = millis();

  int reading = digitalRead(BTN_PIN);
  
  if (reading != lastButtonState) {               // If the switch changed, due to noise or pressing:
    lastDebounceTime = millis();                  // reset the debouncing timer
  }

  if ((millis() - lastDebounceTime) > debounceDelay) {
    if (reading != buttonState) {                 // if the button state has changed:
      buttonState = reading;
    }
  }

  adc_val = readADC_MultipleValues(0);
  float mili_volt = (float)adc_val * 6.144 / 65536 * 2 * 1000;

  cur_val = (mili_volt - VOLTAGE_ZERO) / VOLTAGE_ONE_INCH;

  if (buttonState == LOW) {                       // only toggle the LED if the new button state is LOW
    Serial.println(F("=== Button is pressed, erase data and restart reading === "));
    cnt = 1;
    max_val = cur_val;
    min_val = cur_val;
    acc_val = cur_val;
  }
  else {
    if (max_val < cur_val) max_val = cur_val;
    if (min_val > cur_val) min_val = cur_val;
    acc_val += cur_val;
    cnt ++;
  }

  float avg_val = acc_val / cnt;

  if (millis() - lastPrintTime > 1000){
    Serial.print(F("Voltage: ")); Serial.print(mili_volt); 
    Serial.print(F("mV   CUR: ")); Serial.print(cur_val); 
    Serial.print(F("\"   AVG: ")); Serial.print(avg_val);
    Serial.print(F("\"   MIN: ")); Serial.print(min_val);
    Serial.print(F("\"   MAX: ")); Serial.println(max_val);
    lastPrintTime = millis();
  }

  if (millis() - lastDisplayTime > 200) {
    // Draw values here
    char str_temp[12];
    char t[6];
    dtostrf(cur_val, 3, 1, t);
    sprintf(str_temp, "Cur:%s", t);
    GUI_DisString_EN(0, 0, str_temp, &Font24, FONT_BACKGROUND, WHITE);
    OLED_Display(0, 20, 128, 56);
    OLED_ClearBuf();

    dtostrf(min_val, 3, 1, t);
    sprintf(str_temp, "Min:%s\"", t);
    GUI_DisString_EN(0, 0, str_temp, &Font20, FONT_BACKGROUND, WHITE);
    OLED_Display(0, 56, 128, 78);
    OLED_ClearBuf();
    dtostrf(max_val, 3, 1, t);
    sprintf(str_temp, "Max:%s\"", t);
    GUI_DisString_EN(0, 0, str_temp, &Font20, FONT_BACKGROUND, WHITE);
    OLED_Display(0, 78, 128, 100);
    OLED_ClearBuf();
    dtostrf(avg_val, 3, 1, t);
    sprintf(str_temp, "Avg:%s\"", t);
    GUI_DisString_EN(0, 0, str_temp, &Font20, FONT_BACKGROUND, WHITE);
    OLED_Display(0, 100, 128, 128);
    OLED_ClearBuf();
    
    lastDisplayTime = millis();
  }

  // Delay to ensure 50Hz
  if (millis() - readTime < READ_INTERVAL){
    delay(READ_INTERVAL - millis() + readTime);
  }

  // save the reading. Next time through the loop, it'll be the lastButtonState:
  lastButtonState = reading;
}


/*
  Read multiple(10) values from the ADC and get an average value.
*/
uint16_t readADC_MultipleValues(int ch){
  uint16_t val[10];
  long accum = 0;
  for (int i = 0; i < 10; i++){
    val[i] = ads.readADC_SingleEnded(ch);
    accum += val[i];
  }
  uint16_t max_val = val[0];
  uint16_t min_val = val[0];
  for (int i = 1; i < 10; i++){
    if (val[i] > max_val) max_val = val[i];
    if (val[i] < min_val) min_val = val[i];
  }
  // Remove Max & Min values and get an average value.
  return (accum - max_val - min_val) / 8;
}
