# Individual Sensor Display

## Components

- Arduino Nano
    
    https://www.amazon.com/ELEGOO-Arduino-ATmega328P-without-compatible/dp/B0713XK923

- 1.5" OLED from WaveShare
    
    https://www.waveshare.com/1.5inch-oled-module.htm


- ADS1115 ADC Breakout
    
    https://www.adafruit.com/product/1085
    

- Push Button Switch


## Scenario

- Just read analog voltage from ADS1115, and convert it to inches.

- Record values and display current value, maximum value, and minimum value on the OLED.

- If the button is pressed, clear all data and start over.

## Connection guide

- Breadboard

    ![!Breadboard View](schematic/ISD_bb.jpg)
 
- Schematic
    
    ![Schematic View](schematic/ISD_schem.jpg)
